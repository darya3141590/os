from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
   "https://i.gifer.com/4SHX.gif",
    "https://i.gifer.com/Aw.gif",
    "https://i.gifer.com/SoOk.gif",
    "https://i.gifer.com/PXkM.gif",
    "https://i.gifer.com/g2Pw.gif",
    "https://i.gifer.com/4TVL.gif",
    "https://i.gifer.com/Ar.gif",
    "https://i.gifer.com/ImZK.gif"
    ]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")