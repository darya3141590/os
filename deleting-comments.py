t = open('output.txt', 'w')
with open("letotechnics-config.txt", 'r') as f:
    for line in f:
        check = [(i == "\t") or (i == " ") for i in line.split("#")[0]]
        if (len(set(check)) != 1) or ((check[0] is False) and (len((set(check))) == 1)):    # если до '#' не только (одни пробелы или табы) или пустая строчка, то тогда записываем строчки
            t.write(line)
